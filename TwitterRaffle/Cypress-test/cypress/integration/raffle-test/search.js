describe("sortear una persona según un hashtag", () => {
	it("Ir a la página principal", () => {
		cy.visit('http://demo-bamboo.bit2bitamericas.com:8888/')
	}),
	it("Verificar que la página haya cargado correctamente", () => {
		cy.get('#content > h1 > img').should('be.visible');
	}),
	it("Ingresar hashtag", () => {
		cy.get('#hash').type('#Atlassian', { delay: 100 })
	}),
	it("Buscar tweets", () => {
	    cy.get('#raffleForm > div.text-center > button').click();
	}),
	it("Verificar que la lista de participantes haya cargado", () => {
		cy.get('#casino1 > div').should('be.visible');
	}),
	it("Sortear ganador", () => {
		cy.get('#slotMachineButtonShuffle').click();
	}),
	it("verificar que botón de sorteo desaparezca", () => {
		cy.get('#slotMachineButtonShuffle').should('not.be.visible');
	}),
	it("verificar que sorteo termina", () => {
		cy.get('#slotMachineButtonShuffle',{timeout: 12000}).should('be.visible');
	})
})