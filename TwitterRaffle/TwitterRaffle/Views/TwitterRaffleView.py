# -*- coding: utf-8 -*-
from django.shortcuts import render
import twitter
from twitter.error import TwitterError
from ..settings import EVENT_NAME, CONSUMER_SECRET, ACCESS_TOKEN_KEY, CONSUMER_KEY, ACCESS_TOKEN_SECRET

CONTROL_PARTICIPANTS_NUMBER = 200


def index(request):
    return render(request, 'blog/index.html', {'event_name': EVENT_NAME})


def raffle(request):
    try:
        result = get_participants(request)
        return render(request, 'blog/post_list.html', {"list": result, 'event_name': EVENT_NAME})
    except TwitterError:
        return render(request, 'blog/max_api_uses_error.html', {'event_name': EVENT_NAME})


def exception_error(request):
    return 0 / None


def get_participants(request):
    api = configure_api()
    result = []
    names = []
    last_id = None
    winners_list = request.COOKIES.get('winners', '').split(",")
    while len(result) < CONTROL_PARTICIPANTS_NUMBER:
        api_results = get_results(api, request, last_id)
        if len(api_results) <= 1:
            break
        for item in api_results:
            last_id = add_new_participants(item, names, winners_list, result)
    return result


def configure_api():
    return twitter.Api(consumer_key=CONSUMER_KEY,
                       consumer_secret=CONSUMER_SECRET,
                       access_token_key=ACCESS_TOKEN_KEY,
                       access_token_secret=ACCESS_TOKEN_SECRET)


def add_new_participants(current_item, names, winners, result):
    last_id = current_item.id
    username = map_username(current_item)
    if username not in names and username not in winners:
        new = map_new_user(current_item)
        names.append(new["userName"])
        result.append(new)
    return last_id


def get_results(api, request, last_id):
    return api.GetSearch(term=request.GET.get("hash"), count=300, result_type="recent", max_id=last_id)


def map_new_user(twitter_user):
    return {
        "userDisplay": twitter_user.user.name.encode('utf-8'),
        "image": twitter_user.user.profile_image_url,
        "color": twitter_user.user.profile_link_color,
        "message": twitter_user.text.encode('utf-8'),
        "textColor": twitter_user.user.profile_text_color.encode('utf-8'),
        "userName": map_username(twitter_user)
    }


def map_username(twitter_user):
    return "@" + str(twitter_user.user.screen_name.encode('utf-8'))
