try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python

from django.core.handlers.wsgi import WSGIRequest
from django.test import TestCase

from ..Views.TwitterRaffleView import CONTROL_PARTICIPANTS_NUMBER, index, raffle


class TwitterRaffleTestCase(TestCase):

    def test_fail(self):
        self.assertTrue(True)

    def test_control_participant_number_is_not_none(self):
        self.assertIsNotNone(CONTROL_PARTICIPANTS_NUMBER)
        self.assertTrue(True)

    def test_index_request_returns_request(self):
        request = {}
        result_request = index(request)
        self.assertIsNotNone(result_request)
        self.assertEqual(result_request.status_code, 200)
        self.assertTrue(result_request.has_header("Content-Type"))

    def test_raffle_success(self):
        request = WSGIRequest(
            {'HTTP_COOKIE': 'winners=@user1,@user2', 'REQUEST_METHOD': 'GET', 'QUERY_STRING': 'hash=fake_hash',
             'wsgi.input': StringIO()})
        result_request = raffle(request)
        self.assertIsNotNone(result_request)
        self.assertEqual(result_request.status_code, 200)
