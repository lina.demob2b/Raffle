from django.test import TestCase

from ..Views.TwitterRaffleView import map_new_user, map_username


class TwitterRaffleTestCase(TestCase):

    def test_fail(self):
        self.assertEqual(True, True)


    def test_map_user_name_is_ok(self):
        fake_user = TestBasicGlobalTwitterResponse("username")
        result = map_username(fake_user)
        self.assertEqual("@" + str("username".encode('UTF-8')), result)

    def test_map_user_name_is_wrong(self):
        fake_user = TestBasicGlobalTwitterResponse("username")
        result = map_username(fake_user)
        self.assertNotEqual("username", result)

    def test_map_new_user_is_ok(self):
        fake_user = TestCompleteGlobalTwitterResponse("user")
        result = map_new_user(fake_user)
        self.assertNotEqual(None, result)

    def test_map_new_user_is_wrong(self):
        fake_user = TestBasicGlobalTwitterResponse("user")
        with self.assertRaises(Exception):
            result = map_new_user(fake_user)


class TestBasicGlobalTwitterResponse(object):
    def __init__(self, screen_name):
        self.user = TestBasicUserTwitterResponse(screen_name)


class TestCompleteGlobalTwitterResponse(object):
    def __init__(self, screen_name):
        self.user = TestBasicUserTwitterResponse(screen_name)
        self.text = "message"


class TestBasicUserTwitterResponse(object):
    def __init__(self, screen_name):
        self.screen_name = screen_name
        self.name = screen_name
        self.profile_image_url = "img.jpg"
        self.profile_link_color = "#"
        self.profile_text_color = "red"
