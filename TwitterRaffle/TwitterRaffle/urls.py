from django.conf.urls import url
from TwitterRaffle.Views import TwitterRaffleView

urlpatterns = [
    url(r'^$', TwitterRaffleView.index, name='index'),
    url(r'^raffle', TwitterRaffleView.raffle),
    url(r'^exception-error', TwitterRaffleView.exception_error)
]
